export default class kult4eitemsheet extends ItemSheet{

    getData(){
        const data = super.getData();
        const itemData = data.data;
        data.item = itemData;
        data.data = itemData.data;
        return data;
    }
    get template(){
        return `systems/kult4e/templates/sheets/${this.item.data.type}-sheet.hbs`;
    }

}