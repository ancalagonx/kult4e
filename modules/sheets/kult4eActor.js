export default class kult4eActor extends Actor {

  // TODO: refactor
  async woundEffect(){
    let modifier = 0;
    for (var i=1; i<5; i++){
      if ( getProperty(this.data.data.attributes, `woundtext.majorwound${i}`) && (getProperty(this.data.data.attributes, `woundstabilized.majorwound${i}`) == "false")){
        modifier = 1
      }
    }
    return modifier;
  }  

  displayRollResult({roll, moveName, resultText, moveResultText }) {
    ChatMessage.create({ 
      content: `
        <div class='move-name'>${moveName}</div>
        <div class='move-name'>${resultText}!</div>
        <div class='move-result'>${moveResultText}</div>
        <div class='result-roll'>
          <div class='tooltip'>
            ${roll.total}
            <span class='tooltiptext'>${roll.result}</span>
          </div>
        </div>`,
      speaker: ChatMessage.getSpeaker({ alias: this.name })
    });
  };

  async moveroll(moveName){
    const actordata = this.data;
    kultLogger("Actor Data => ", actordata);

    let move = actordata.items.find((item) => item.name == moveName);
    kultLogger("Move => ", move);

    const moveData = move.data.data;
    const moveType = moveData.type;
    kultLogger("Move Type => ", moveType);

    if (moveType === "passive") {
      ui.notifications.warn(game.i18n.localize("kult4e.PassiveAbility"))
    } else {
      const attr = moveData.attributemod;
      const successtext = moveData.completesuccess;
      const failuretext = moveData.failure;
      const partialsuccess = moveData.partialsuccess;
      const specialflag = moveData.specialflag;
      let mod = 0;
      let harm = 0;
      if (specialflag == 3) { // Endure Injury
        let boxoutput = await new Promise(resolve => {
          new Dialog({
            title: game.i18n.localize("kult4e.EndureInjury"),
            content: `<div class="endure-harm-dialog"><label>${game.i18n.localize("kult4e.EndureInjuryDialog")}</label><input id="harm_value" data-type="number" type="number"></div>`,
            default: 'one',
            buttons: {
              one: {
                label: "Ok",
                callback: () => {
                  resolve({ "harm_value": document.getElementById("harm_value").value })
                }
              }
            }
          }).render(true);
        })
        harm = boxoutput.harm_value;
      }

      if (attr != '') {
        mod = actordata.data.attributes[attr];
      }
      
      let stab = actordata.data.stability.value;
      let situation = parseInt(actordata.data.sitmod) + parseInt(actordata.data.forward);
      kultLogger("Sitmod => ", actordata.data.sitmod);

      let woundmod = await this.woundEffect();
      situation -= woundmod;

      if (actordata.data.attributes.criticalwound && actordata.data.attributes.criticalwoundstabilized != "true") { 
        situation -= 1;
      }
      if (specialflag == 1 && stab > 2) {
        situation -= 1
      };
      if (moveType == "disadvantage" && stab > 0) {
        situation -= 1
      };
      if (moveType == "disadvantage" && stab > 2) {
        situation -= 1
      };
      if (specialflag == 1 && stab > 5) {
        situation -= 1
      };
      if (moveType == "disadvantage" && stab > 5) {
        situation -=1
      };
      if (specialflag == 2 && stab > 5) {
        situation += 1
      }; 
    
      kultLogger("Attribute Mod => ", mod);
      kultLogger("Situation Mod => ", situation);
      kultLogger("Harm => ", harm);

      let r = new Roll(`2d10 + ${mod} + ${situation} - ${harm}`);
      r.roll()

      if (game.dice3d){
        await game.dice3d.showForRoll(r);
      }

      if(r.total){
        kultLogger("Roll Successful");
        this.update({"data.sitmod": 0});
        kultLogger(`Sitmod is ` + this.data.data.sitmod);
      }

      if (r.total >= 15) {
        this.displayRollResult({ roll: r, moveName, resultText:  game.i18n.localize("kult4e.Success"), moveResultText: successtext });
      }
      else if (r.total < 10) {
        this.displayRollResult({ roll: r, moveName, resultText: game.i18n.localize("kult4e.Failure"), moveResultText: failuretext });
      }
      else {
        this.displayRollResult({ roll: r, moveName, resultText: game.i18n.localize("kult4e.PartialSuccess"), moveResultText: partialsuccess });
      }
    }   
  }
}
